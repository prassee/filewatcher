package prassee

import java.nio.file._
import java.util
import java.util.List

object FileWatcher extends App {
  val watcher = FileSystems.getDefault.newWatchService()
  val watchedDir = Paths.get("/home/ubuntu/watched")

  watchedDir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE,
    StandardWatchEventKinds.ENTRY_MODIFY,
    StandardWatchEventKinds.OVERFLOW,
    StandardWatchEventKinds.ENTRY_DELETE)

  while (true) {
    val wk = watcher.take()
    val x: util.List[WatchEvent[_]] = wk.pollEvents()
    x.toArray.foreach(x => {
      val y = x.asInstanceOf[WatchEvent[Path]]
      y.kind() match {
        case StandardWatchEventKinds.ENTRY_CREATE => println("MPS folder Created - " + y.context().getFileName)
        case _ => println("Some other event")
      }
    }
    )
    wk.reset()
  }

}